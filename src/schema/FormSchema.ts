import * as Yup from "yup";

export default Yup.object().shape({
  name: Yup.string().required("Campo obrigatório").min(3, "Nome inválido"),
  email: Yup.string().required("Campo obrigatório").email("Email inválido"),
  cpf: Yup.string().required("Campo obrigatório").max(11, "CPF inválido"),
  anonas: Yup.string().required("Campo obrigatório"),
  telefone: Yup.string()
    .required("Campo obrigatório")
    .max(15, "Número inválido"),
  instagram: Yup.string()
    .required("Campo obrigatório")
    .test("teste-instagram", "User inválido", (user) => {
      if (user?.startsWith("@")) {
        return true;
      }
      return false;
    }),
  terms: Yup.bool().oneOf([true], "Marcação obrigatória"),
});
