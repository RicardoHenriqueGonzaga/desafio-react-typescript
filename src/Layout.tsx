import { PropsWithChildren } from "react";

import Header from "./components/Header";

import Breadcrumb from "./components/Breadcrumb";

import Newsletter from "./components/Newsletter";

import Footer from "./components/Footer";

export default function Layout({ children }: PropsWithChildren) {
  return (
    <>
      <Header />

      <Breadcrumb />

      <main>{children}</main>

      <Newsletter />

      <Footer />
    </>
  );
}
