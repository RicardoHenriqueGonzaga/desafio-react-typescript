import HomeIcon from "../../assets/home-icon.svg";
import ArrowIcon from "../../assets/arrow-right-icon.svg";
import styles from "./breadcrumb.module.css";

export default function Breadcrumb() {
  return (
    <div className="container">
      <ul className={styles["breadcrumb-list"]}>
        <li>
          <img src={HomeIcon} alt="voltar ínicio" />
        </li>
        <li>
          <img src={ArrowIcon} alt="Seta para direita" />
        </li>
        <li>Institucional</li>
      </ul>
    </div>
  );
}
