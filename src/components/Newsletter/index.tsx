import styles from "./newsletter.module.css";

export default function Newsletter() {
  return (
    <div className={styles["newsletter-content"]}>
      <div className="container">
        <h5 className={styles["newsletter-message"]}>
          Assine nossa newsletter
        </h5>

        <input
          className={styles["newsletter-input"]}
          type="text"
          name="E-mail"
          id="Email"
          placeholder="E-mail"
        />

        <button className={styles["newsletter-button"]}>Enviar</button>
      </div>
    </div>
  );
}
