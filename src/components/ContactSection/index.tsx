import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import FormSchema from "../../schema/FormSchema";
import { string } from "yup";

import styles from "./contact.module.css";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  anonas: string;
  telefone: string;
  instagram: string;
  terms: boolean;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  anonas: "",
  telefone: "",
  instagram: "",
  terms: false,
};

export default function ContactSection() {
  const handleFormikSubmit = (values: IFormikValues) => {};
  return (
    <>
      <h2 className={styles["subtitle-contato"]}>Preencha o formulário</h2>
      <div className={styles["form-wrapper"]}>
        <Formik
          onSubmit={(values, { resetForm }) => {
            handleFormikSubmit(values);
            resetForm();
          }}
          initialValues={initialValues}
          validationSchema={FormSchema}
        >
          {({ errors, touched }) => (
            <Form>
              <div className={styles["form-col"]}>
                <label htmlFor="name">Nome</label>
                <Field
                  id="name"
                  name="name"
                  placeholder="Seu nome completo"
                  className={
                    errors.name && touched.name && `${styles["invalid"]}`
                  }
                />
                <ErrorMessage
                  component="span"
                  name="name"
                  className={styles["form-invalid-feedback"]}
                />
              </div>
              <div className={styles["form-col"]}>
                <label htmlFor="email">E-mail</label>
                <Field
                  id="email"
                  name="email"
                  placeholder="Seu e-mail"
                  className={
                    errors.email && touched.email && `${styles["invalid"]}`
                  }
                />{" "}
                <ErrorMessage
                  component="span"
                  name="email"
                  className={styles["form-invalid-feedback"]}
                />
              </div>
              <div className={styles["form-col"]}>
                <label htmlFor="cpf">CPF</label>
                <Field
                  id="cpf"
                  name="cpf"
                  placeholder="000 000 000 00"
                  className={
                    errors.cpf && touched.cpf && `${styles["invalid"]}`
                  }
                />
                <ErrorMessage
                  component="span"
                  name="cpf"
                  className={styles["form-invalid-feedback"]}
                />
              </div>
              <div className={styles["form-col"]}>
                <label htmlFor="anonas">Data de Nascimento:</label>
                <Field
                  id="anonas"
                  name="anonas"
                  placeholder="00 00 0000"
                  className={
                    errors.anonas && touched.anonas && `${styles["invalid"]}`
                  }
                />
                <ErrorMessage
                  component="span"
                  name="anonas"
                  className={styles["form-invalid-feedback"]}
                />
              </div>
              <div className={styles["form-col"]}>
                <label htmlFor="telefone">Telefone:</label>
                <Field
                  id="telefone"
                  name="telefone"
                  placeholder="(+00) 00000 0000"
                  className={
                    errors.telefone &&
                    touched.telefone &&
                    `${styles["invalid"]}`
                  }
                />
                <ErrorMessage
                  component="span"
                  name="telefone"
                  className={styles["form-invalid-feedback"]}
                />
              </div>
              <div className={styles["form-col"]}>
                {" "}
                <label htmlFor="instagram">Instagram</label>
                <Field
                  id="instagram"
                  name="instagram"
                  placeholder="@seuuser"
                  className={
                    errors.instagram &&
                    touched.instagram &&
                    `${styles["invalid"]}`
                  }
                />
                <ErrorMessage
                  component="span"
                  name="instagram"
                  className={styles["form-invalid-feedback"]}
                />
                <div className={styles["terms-wrapper"]}>
                  <label htmlFor="terms">
                    <span style={{ color: "#03BFFE" }}>*</span>Declaro que li e
                    aceito
                  </label>
                  <Field type="checkbox" name="terms" id="terms" />
                </div>
              </div>

              <button type="submit">Cadastre-se</button>
            </Form>
          )}
        </Formik>
      </div>
    </>
  );
}
