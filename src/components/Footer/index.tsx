import styles from "./footer.module.css";

import FacebookIcon from "../../assets/facebook-icon.svg";
import InstagramIcon from "../../assets/instagram-icon.svg";
import TwitterIcon from "../../assets/twitter-icon.svg";
import YoutubeIcon from "../../assets/youtube-icon.svg";
import LinkedinIcon from "../../assets/linkedin-icon.svg";
import MastercardIcon from "../../assets/mastercard-icon.svg";
import VisaIcon from "../../assets/visa-icon.svg";
import AmericanexpressIcon from "../../assets/americanexpress-icon.svg";
import EloIcon from "../../assets/elo-icon.svg";
import HipercardIcon from "../../assets/hipercard-icon.svg";
import PaypalIcon from "../../assets/paypal-icon.svg";
import BoletoIcon from "../../assets/boletobancario-icon.svg";
import VtexpciIcon from "../../assets/vtexpci-icon.svg";
import VtexIcon from "../../assets/vtex-icon.svg";
import LogoM3 from "../../assets/logom3-icon.svg";

export default function Footer() {
  return (
    <footer className={styles["footer"]}>
      <div className="container">
        <div className={styles["footer-content"]}>
          <div className={styles["footer-lists"]}>
            <ul className={styles["footer-list"]}>
              Institucional <span>+</span>
              <li>
                <a href="/">Quem Somos</a>
              </li>
              <li>
                <a href="/">Política de Privacidade</a>
              </li>
              <li>
                <a href="/">Segurança</a>
              </li>
              <li>
                <a href="/">
                  <u>Seja um Revendedor</u>
                </a>
              </li>
            </ul>
            <ul className={styles["footer-list"]}>
              Dúvidas <span>+</span>
              <li>
                <a href="/">Entrega</a>
              </li>
              <li>
                <a href="/">Pagamento</a>
              </li>
              <li>
                <a href="/">Trocas e Devoluções</a>
              </li>
              <li>
                <a href="/">Dúvidas Frequentes</a>
              </li>
            </ul>
            <ul className={styles["footer-list"]}>
              Fale conosco <span>+</span>
              <li>
                <strong>Atendimento ao Consumidor</strong>
              </li>
              <li>(11) 4159 9504</li>
              <li>
                <strong>Atendimento Online</strong>
              </li>
              <li>(11) 99433-8825</li>
            </ul>
          </div>

          <div className={styles["footer-icons"]}>
            <ul>
              <li>
                <a href="/">
                  <img src={FacebookIcon} alt="Ícone do facebook" />
                </a>
              </li>
              <li>
                <a href="/">
                  <img src={InstagramIcon} alt="Ícone do instagram" />
                </a>
              </li>
              <li>
                <a href="/">
                  <img src={TwitterIcon} alt="Ícone do twitter" />
                </a>
              </li>
              <li>
                <a href="/">
                  <img src={YoutubeIcon} alt="Ícone do youtube" />
                </a>
              </li>
              <li>
                <a href="/">
                  <img src={LinkedinIcon} alt="Ícone do linkedin" />
                </a>
              </li>
            </ul>
            <p>www.loremipsum.com</p>
          </div>
        </div>
      </div>
      <div className={styles["footer-copyright"]}>
        <span className={styles["desktop-only"]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </span>
        <div className={styles["copyright-icons"]}>
          <ul>
            <li>
              <img src={MastercardIcon} alt="Ícone do mastercard" />
            </li>
            <li>
              <img src={VisaIcon} alt="Ícone do visa" />
            </li>
            <li>
              <img src={AmericanexpressIcon} alt="Ícone do americanexpress" />
            </li>
            <li>
              <img src={EloIcon} alt="Ícone do elo" />
            </li>
            <li>
              <img src={HipercardIcon} alt="Ícone do hipercard" />
            </li>
            <li>
              <img src={PaypalIcon} alt="Ícone do paypal" />
            </li>
            <li className={styles["border-boleto"]}>
              <img src={BoletoIcon} alt="Ícone do boleto bancário" />
            </li>
            <li>
              <img src={VtexpciIcon} alt="Ícone do vtex/pci" />
            </li>
          </ul>
        </div>
        <p className={styles["copyright-paragraph"]}>
          <span className={styles["mobile-only"]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. .
          </span>
          <div className={styles["poweredby"]}>
            <div className={styles["text-poweredby"]}>
              Powered by
              <img width={44} height={16} src={VtexIcon} alt="Ícone do Vtex" />
            </div>
            Developed by
            <img
              className={styles["logom3-footer"]}
              width={28}
              height={16}
              src={LogoM3}
              alt="ícone logo da m3"
            />
          </div>
        </p>
      </div>
    </footer>
  );
}
