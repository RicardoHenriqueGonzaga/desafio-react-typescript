import MenuIcon from "../../../assets/menu-mobile.svg";
import { useState } from "react";

import styles from "./menu.module.css";

export default function Menu() {
  const [showMenu, setShowMenu] = useState(false);
  return (
    <>
      <button
        onClick={() => setShowMenu(true)}
        className={styles["menu-mobile-button"]}
      >
        <img src={MenuIcon} />
      </button>
      {showMenu && (
        <div className={styles["menu-mobile"]}>
          <button onClick={() => setShowMenu(false)}>X</button>

          <ul>
            <li>
              <a href="/" className={styles["menu-link"]}>
                Cursos
              </a>
            </li>
            <li>
              <a href="/" className={styles["menu-link"]}>
                Saiba Mais
              </a>
            </li>
          </ul>
        </div>
      )}
    </>
  );
}
