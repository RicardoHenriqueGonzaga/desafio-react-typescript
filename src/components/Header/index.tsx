import styles from "./header.module.css";

import Logo from "../../assets/logo.svg";
import Menu from "./Menu";
import CartIcon from "../../assets/cart-icon.svg";
import SearchIcon from "../../assets/search-icon.svg";

export default function Header() {
  return (
    <header className={styles["header"]}>
      <div className={styles["container"]}>
        <section className={styles["header-main"]}>
          <Menu />
          <a href="/">
            <img className={styles["logo"]} alt="logo-m3-academy" src={Logo} />
          </a>

          <div
            className={`${styles["search-wrapper"]} ${styles["search-desktop"]}`}
          >
            <input
              className={styles["search"]}
              type="text"
              name="search"
              id="search"
              placeholder="Buscar..."
            />
            <img
              className={styles["search-icon"]}
              src={SearchIcon}
              alt="ícone-lupa"
            />
          </div>

          <div className={styles["header-buttons"]}>
            <a href="/" className={styles["button-login"]}>
              Entrar
            </a>

            <button className={styles["cart-icon"]}>
              <img src={CartIcon} />
            </button>
          </div>
        </section>

        <section className={styles["search-wrapper"]}>
          <input
            className={styles["search"]}
            type="text"
            name="search"
            id="search"
            placeholder="Buscar..."
          />
          <img
            className={styles["search-icon"]}
            src={SearchIcon}
            alt="ícone-lupa"
          />
        </section>
      </div>

      <nav className={styles["header-menu-desktop"]}>
        <ul className={styles["container"]}>
          <li>
            <a href="/" className={styles["menu-link"]}>
              Cursos
            </a>
          </li>
          <li>
            <a href="/" className={styles["menu-link"]}>
              Saiba Mais
            </a>
          </li>
        </ul>
      </nav>
    </header>
  );
}
