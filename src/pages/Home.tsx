import { useState } from "react";
import Layout from "../Layout";
import styles from "./Home.module.css";

import WppIcon from "../assets/whatsapp-icon.svg";
import ArrowTopIcon from "../assets/arrow-top-icon.svg";
import homeSections from "../utils/home.sections";
import ContactSection from "../components/ContactSection";

function Home() {
  const [currentSection, setCurrentSection] = useState(homeSections[0]);

  return (
    <Layout>
      <section className="container">
        <h1 className={styles["title"]}>Institucional</h1>
        <div className={styles["content-wrapper"]}>
          <div className={styles["content-list"]}>
            <ul>
              {homeSections.map((section, index) => (
                <li
                  onClick={() => setCurrentSection(section)}
                  className={
                    homeSections.indexOf(currentSection) === index
                      ? styles["active"]
                      : ""
                  }
                >
                  {section.title}
                </li>
              ))}
            </ul>
          </div>

          <div className={styles["subtitle-adjust"]}>
            {" "}
            {currentSection.type === "contact" ? (
              <ContactSection />
            ) : (
              <>
                <h2 className={styles["subtitle"]}>{currentSection.title}</h2>
                <p
                  className={styles["text-content"]}
                  dangerouslySetInnerHTML={{ __html: currentSection.content }}
                />
              </>
            )}
          </div>
        </div>
      </section>
      <div className={styles["info-icons"]}>
        <img
          className={styles["wpp-icon"]}
          width="34"
          height="34"
          src={WppIcon}
          alt="Ícone do Whatsapp"
        />
        <img
          width="37"
          height="37"
          src={ArrowTopIcon}
          alt="Ícone seta pra cima"
        />
      </div>
    </Layout>
  );
}

export default Home;
